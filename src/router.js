import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            alias: '/persons',
            name: 'persons',
            component: () => import('./components/PersonList')
        },
        {
            path: '/persons/:id',
            name: 'edit-person',
            component: () => import('./components/EditPerson')
        }, {
            path: '/addperson',
            name: 'add-person',
            component: () => import('./components/AddPerson')
        }
    ]
})