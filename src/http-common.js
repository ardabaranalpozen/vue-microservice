import axios from 'axios'

export default axios.create({
    //baseURL: "http://localhost:8082/api",
    //baseURL: "http://192.168.49.2:32345/api",
    baseURL: "http://backend-svc:8082/api",

    headers: {
        'Content-Type': 'application/json'
    }
})
