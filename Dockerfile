
### STAGE 1: Build ###
FROM node:14 AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
WORKDIR /usr/src/app
RUN npm run build

### STAGE 2: Run ###
FROM nginx:latest
COPY default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /usr/src/app/dist/ /usr/share/nginx/html
# --------------------------------------------------------------------------------------------------

# FROM node:lts-alpine
# RUN npm install -g http-server
# WORKDIR /app
# COPY package*.json ./
# RUN npm install
# COPY . .
# RUN npm run build
# EXPOSE 8080
# CMD [ "http-server", "dist" ]

